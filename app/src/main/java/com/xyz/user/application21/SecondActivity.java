package com.xyz.user.application21;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SecondActivity extends AppCompatActivity {

    TextView textViewYears;
    TextView textViewDays;
    TextView textViewSeconds;
    TextView textViewZodiak;
    TextView textViewUserName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        textViewUserName = (TextView) findViewById(R.id.textViewUserName);
        textViewYears = (TextView) findViewById(R.id.textViewYears);
        textViewDays = (TextView) findViewById(R.id.textViewDays);
        textViewSeconds = (TextView) findViewById(R.id.textViewSeconds);
        textViewZodiak = (TextView) findViewById(R.id.textViewZodiak);


        if (getIntent().hasExtra(MainActivity.nameUser)) {
            textViewUserName.setText(getIntent().getStringExtra(MainActivity.nameUser));
        }

        if (getIntent().hasExtra(MainActivity.yearsUser)) {
            textViewYears.setText(getIntent().getStringExtra(MainActivity.yearsUser));
        }

        if (getIntent().hasExtra(MainActivity.daysUser)) {
            textViewDays.setText(getIntent().getStringExtra(MainActivity.daysUser));
        }

        if (getIntent().hasExtra(MainActivity.secondsUser)) {
            textViewSeconds.setText(getIntent().getStringExtra(MainActivity.secondsUser));
        }

        if (getIntent().hasExtra(MainActivity.zodiacUser)) {
            textViewZodiak.setText(getIntent().getStringExtra(MainActivity.zodiacUser));
        }


    }
}
