package com.xyz.user.application21;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/*
1. Создать приложение с двумя экранами. На первом экране пользователь вводит свое имя и дату рождения
и нажимает кнопку “продолжить”. Затем открывается второй экран, на котором пользователь видит
 свою статистику по прожитому времени (сколько ему лет, сколько дней он прожил, сколько секунд
 ему исполнилось в момент открытия статистики) + его знак зодиака.

 */
public class MainActivity extends AppCompatActivity {

    Button nextButton;
    public static final String nameUser = "name";
    public static final String yearsUser = "years";
    public static final String daysUser = "days";
    public static final String secondsUser = "seconds";
    public static final String zodiacUser = "zodiac";


    EditText nameText;
    EditText dateText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        nextButton = (Button) findViewById(R.id.button);
        nameText = (EditText) findViewById(R.id.editTextName);
        dateText = (EditText) findViewById(R.id.editTextDate);


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get current date
                //Sun Dec 24 20:45:54 GMT+02:00 2017
                Date curDate = new Date();

                //get user date
                //String str = "20/03/1978/00:00:00";
                String str = dateText.getText().toString() + "/00:00:00";

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy/HH:mm:ss");

                Date userDate;
                try {
                    userDate = format.parse(str);
                } catch (ParseException exp) {

                    userDate = new Date();
                }
                //calculate
                long diff = curDate.getTime() - userDate.getTime();

                long years = TimeUnit.MILLISECONDS.toDays(diff) / 365;
                long days = TimeUnit.MILLISECONDS.toDays(diff);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);

                String zodiac = calculateZodiac(str);

                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra(nameUser, nameText.getText().toString());
                intent.putExtra(yearsUser, Long.toString(years));
                intent.putExtra(daysUser, Long.toString(days));
                intent.putExtra(secondsUser, Long.toString(seconds));
                intent.putExtra(zodiacUser, zodiac);

                startActivity(intent);
            }
        });

    }


    String calculateZodiac(String str) {

        //String str = "20/03/1978/00:00:00";
        String tempStr[] = str.split("/");

        int day = Integer.parseInt(tempStr[0]);
        int month = Integer.parseInt(tempStr[1]);

        String zodiacStr = "";
        if ((month == 12 && day >= 22 && day <= 31) || (month == 1 && day >= 1 && day <= 19))
            zodiacStr = "Козерог";
        else if ((month == 1 && day >= 20 && day <= 31) || (month == 2 && day >= 1 && day <= 17))
            zodiacStr = "Водолей";
        else if ((month == 2 && day >= 18 && day <= 29) || (month == 3 && day >= 1 && day <= 20))
            zodiacStr = "Рыбы";
        else if ((month == 3 && day >= 20 && day <= 31) || (month == 4 && day >= 1 && day <= 21))
            zodiacStr = "Овен";
        else if ((month == 4 && day >= 20 && day <= 30) || (month == 5 && day >= 1 && day <= 20))
            zodiacStr = "Телец";
        else if ((month == 5 && day >= 21 && day <= 31) || (month == 6 && day >= 1 && day <= 20))
            zodiacStr = "Близнецы";
        else if ((month == 6 && day >= 21 && day <= 30) || (month == 7 && day >= 1 && day <= 22))
            zodiacStr = "Рак";
        else if ((month == 7 && day >= 23 && day <= 31) || (month == 8 && day >= 1 && day <= 22))
            zodiacStr = "Лев";
        else if ((month == 8 && day >= 23 && day <= 31) || (month == 9 && day >= 1 && day <= 22))
            zodiacStr = "Дева";
        else if ((month == 9 && day >= 23 && day <= 30) || (month == 10 && day >= 1 && day <= 22))
            zodiacStr = "Весы";
        else if ((month == 10 && day >= 23 && day <= 31) || (month == 11 && day >= 1 && day <= 21))
            zodiacStr = "Скорпион";
        else if ((month == 11 && day >= 22 && day <= 30) || (month == 12 && day >= 1 && day <= 21))
            zodiacStr = "Стрелец";

        return zodiacStr;

    }

}
